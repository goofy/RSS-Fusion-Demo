<?php
	//~ Include RSS Fusion
	require_once 'vendor/RSS-Fusion/index.php';

	//~ Check if conf file exist
	if(!array_key_exists('c', $_GET) 
		OR !isset($_GET['c'])
			OR !file_exists(TL_ROOT . '/../../c/'.$_GET['c'])){
		exit;
	}

	$conf = TL_ROOT . '/../../c/'.$_GET['c'];

	//~ Update file date (file is deleted if is not updated every 30 days)
	touch($conf);

	//~ Open conf
	$oConf = json_decode(file_get_contents($conf));

	if($oConf && gettype($oConf) == 'object'){
		$_links = $oConf->flux;

		//~ Set config
		\Config::set('where', $oConf->where);
		\Config::set('_starWords', $oConf->_starWords);
		\Config::set('_badWords', $oConf->_badWords);

		//~ Load feeds
		$oFeeds = new FeedReader($_links);
		
		if(!is_null($oFeeds->objParseFeed)){
			$_items = array();
			$_c     = array();

  			foreach($oFeeds->objParseFeed->items as $item){
  				if(!empty($item['link']) && !in_array($item['link'], $_c)){
  					//~ Delete duplicate item by link
  					array_push($_c, $item['link']);

  					//~ Keywords filter
  					if((count(Config::get('_starWords')) && $item['show'] == 'star') 
  						OR (!count(Config::get('_starWords')) && $item['show'] == 'show')){
  						array_push($_items, $item);
  					}
  				}
  			}
		}

		//~ Generate RSS
		$rssfeed = '<?xml version="1.0" encoding="UTF-8"?>';
	   		$rssfeed .= '<rss version="2.0">';
	    		$rssfeed .= '<channel>';
	    			$rssfeed .= '<title>'.($oConf->title && !empty($oConf->title) ? $oConf->title : 'Flux personnalisé RSS Fusion').'</title>';
	    			$rssfeed .= '<link>'.($oConf->link && !empty($oConf->link) ? $oConf->link : 'https://framagit.org/Erase/RSS-Fusion-demo').'</link>';
					$rssfeed .= '<description>'.($oConf->description && !empty($oConf->description) ? $oConf->description : 'Flux généré avec RSS Fusion').'</description>';

	    if(count($_items)){
	    	foreach ($_items as $item){
	    		$rssfeed .= '<item>';
		    		$rssfeed .= '<title>'.html_entity_decode($item['title']).'</title>';
		    		$rssfeed .= '<link>'.$item['link'].'</link>';
		    		$rssfeed .= '<description><![CDATA['.$item['description'].']]></description>';
		    		$rssfeed .= '<pubDate>'.date('r', $item['pubdate']).'</pubDate>';
		    		$rssfeed .= '<category>'.$item['category'].'</category>';
		    		$rssfeed .= '<enclosure url="'.$item['enclosure'].'" />';

		    		$rssfeed .= '<permalink>'.$item['permalink'].'</permalink>';
		    		$rssfeed .= '<show>'.$item['show'].'</show>';
		    		$rssfeed .= '<base>'.$item['base'].'</base>';
	    		$rssfeed .= '</item>';
	    	}
	    }

	    		$rssfeed .= '</channel>';
    		$rssfeed .= '</rss>';


    	//~ Return RSS content
    	header('Content-type: application/xml');
		echo $rssfeed;
	}
	